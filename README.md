# README #

This repository is a n implementation of the A* path planning algorithm.
The structure of the code matches the requirements of the [global planner](http://wiki.ros.org/global_planner) plugin of the [ROS node **move_base**](http://wiki.ros.org/move_base).

The main feature that makes this implementation different is that it allows the plans to be out of the global costmap.
Out of the map, the unknown space is considered as open space.
This feature permits exploration tasks.

An optional feature is smoothing.  Smoothing the path transforms the cell-based path into segment-based path using line of sight method.
The outcome is a path composed by the extreme points of the segments.

### Parameters ###
* smooth (boolean) indicates if the path is smoothed or not (false by default).
* max_distance (float) forces the smooth process to generate intermediate points so that the segments are no longer than max_distance (default value is 5.0 m).

### Contact ###
Author and manteiner: Pablo Urcola urcola@unizar.es