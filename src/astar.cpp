#include <boost/utility.hpp>
#include <opencv/highgui.h>
#include <pluginlib/class_list_macros.h>
#include <nav_msgs/Path.h>
#include <opencv/cv.h>

#include "astar.h"
#include "state.h"

//register this planner as a BaseGlobalPlanner plugin
PLUGINLIB_DECLARE_CLASS(astar, Astar, astar::Astar, nav_core::BaseGlobalPlanner)

astar::Astar::Astar(): initialized_(false)
{}

void astar::Astar::initialize(std::string name, costmap_2d::Costmap2DROS* costmap_ros)
{
    ros::NodeHandle public_node;
    plan_publisher_ = public_node.advertise<nav_msgs::Path>("path", 10);
    costmap_ros_ = costmap_ros;

    //leer parametros por name
    ros::NodeHandle private_node("~/" + name);
    private_node.param("max_distance", max_distance_, 5.0);
    private_node.param("smooth", smoothed_, false);

    initialized_ = true;
}


namespace
{
struct Cost
{
    const cv::Mat1f& distances_;
    double radius_;

    Cost(const cv::Mat1f& distances, double radius):
        distances_(distances), radius_(radius)
    {
    }

    double operator()(const State& from, const State& to) const
    {
        StateDistance distance;
        //*
        if (to.x >= 0 and to.y < distances_.cols and to.y >= 0 and to.y < distances_.rows)
        {

            double cost = std::pow(distances_(cv::Point(to.x, to.y)) - radius_, -0.1);
            if (cost < 0)
                cost = std::numeric_limits<double>::infinity();

            return distance(from, to)*cost;
        }
        //*/
        return distance(from, to);
    }
};

struct Admissible
{
    const cv::Mat1f& distances_;
    double radius_;

    Admissible(const cv::Mat1f& distances, double radius):
        distances_(distances), radius_(radius)
    {}

    bool operator()(const State& s) const
    {
        if (s.x >= 0 and s.y < distances_.cols and s.y >= 0 and s.y < distances_.rows)
            return distances_(cv::Point(s.x, s.y)) > radius_;

        return true;
    }
};
}

bool astar::Astar::makePlan(const geometry_msgs::PoseStamped& start,
                            const geometry_msgs::PoseStamped& goal, std::vector<geometry_msgs::PoseStamped>& plan)
{

    if (not initialized_)
    {
        ROS_WARN("Unable to make a plan before initialization");
        return false;
    }
    if (start.header.frame_id != costmap_ros_->getGlobalFrameID() or goal.header.frame_id != costmap_ros_->getGlobalFrameID())
    {
        ROS_WARN("Start and Goal points must be in the global costmap reference frame");
        return false;
    }

    //get map
    costmap_2d::Costmap2D costmap;
    costmap_ros_->getCostmapCopy(costmap);

    //compute start state
    State start_state;
    costmap.worldToMapNoBounds(start.pose.position.x, start.pose.position.y, start_state.x, start_state.y);

    //compute goal state
    State goal_state;
    costmap.worldToMapNoBounds(goal.pose.position.x, goal.pose.position.y, goal_state.x, goal_state.y);

    if (not admissible(start_state, costmap))
    {
        ROS_WARN("Start state is not admissible");
        return false;
    }

    if (not admissible(goal_state, costmap))
    {
        ROS_WARN("Goal state is not admissible");
        return false;
    }

    std::list<State> path;
    computePath(start_state, goal_state, boost::bind(&Astar::cost, this, _1, _2, costmap), boost::bind(&Astar::admissible, this, _1, costmap), StateDistance(), path);

    if (smoothed_)
        path = smooth(path, costmap);

    //convert path into plan
    for (std::list<State>::const_iterator cell = path.begin(); cell != path.end(); ++cell)
    {
        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = costmap_ros_->getGlobalFrameID();
        pose.pose.position.x = costmap.getOriginX() + (cell->x + 0.5)*costmap.getResolution();
        pose.pose.position.y= costmap.getOriginY() + (cell->y + 0.5)*costmap.getResolution();
        plan.push_back(pose);
    }

    plan.front() = start;
    plan.back() = goal;

    for (unsigned int i = 1; i < plan.size() - 1; ++i)
    {
        double yaw = std::atan2(plan[i].pose.position.y - plan[i-1].pose.position.y, plan[i].pose.position.x - plan[i-1].pose.position.x);
        plan[i].pose.orientation = tf::createQuaternionMsgFromYaw(yaw);
    }

    publishPath(plan);

    return true;
}



void astar::Astar::publishPath(const std::vector<geometry_msgs::PoseStamped>& plan)
{
    nav_msgs::Path path_msgs;

    path_msgs.header = plan.front().header;
    path_msgs.poses = plan;

    plan_publisher_.publish(path_msgs);

}

unsigned char astar::Astar::cost(const State& s, const costmap_2d::Costmap2D& costmap) const
{
    int size_x = costmap.getSizeInCellsX();
    int size_y = costmap.getSizeInCellsY();
    if (s.x >= 0 and s.x < size_x and s.y >= 0 and s.y < size_y)
        return costmap.getCost(s.x, s.y);

    return costmap_2d::FREE_SPACE;
}

double astar::Astar::cost(const State& from, const State& to, const costmap_2d::Costmap2D& costmap) const
{
    StateDistance distance;
    int size_x = costmap.getSizeInCellsX();
    int size_y = costmap.getSizeInCellsY();
    if (to.x >= 0 and to.x < size_x and to.y >= 0 and to.y < size_y)
    {
        int cell_cost = costmap.getCost(to.x, to.y);
        return distance(from, to)*(1 + cell_cost);
    }

    return distance(from, to);
}

bool astar::Astar::admissible(const State& s, const costmap_2d::Costmap2D& costmap) const
{
    unsigned char c = cost(s, costmap);
    return c != costmap_2d::INSCRIBED_INFLATED_OBSTACLE and c != costmap_2d::LETHAL_OBSTACLE;
}

void astar::Astar::computeInputs(const std::vector<geometry_msgs::PoseStamped>& plan, std::vector<tf::Point>& input) const
{
    input.resize(plan.size());

    for (unsigned int i = 1; i < plan.size(); ++i)
    {
        tf::Point current_pose, next_pose;
        tf::pointMsgToTF(plan[i-1].pose.position, current_pose);
        tf::pointMsgToTF(plan[i].pose.position, next_pose);

        tf::Quaternion current_orientation, next_orientation;
        tf::quaternionMsgToTF(plan[i-1].pose.orientation, current_orientation);
        tf::quaternionMsgToTF(plan[i].pose.orientation, next_orientation);

        double current_yaw = tf::getYaw(current_orientation);
        tf::Point delta_pose = next_pose - current_pose;
        double delta_yaw = next_orientation.angleShortestPath(current_orientation);

        double delta_s = delta_pose.getX()*std::cos(current_yaw) + delta_pose.getY() *std::sin(current_yaw);

        input[i - 1] = tf::Point(1.0, delta_yaw/delta_s, (-delta_pose.getX()*std::sin(current_yaw) + delta_pose.getY()*std::cos(current_yaw))/delta_s);
    }

    input.back().setZero();
}


std::list<State> astar::Astar::smooth(const std::list<State>& path, const costmap_2d::Costmap2D& costmap) const
{
    std::list<State> out;

    std::list<State>::const_iterator current = path.begin();
    while (current != path.end())
    {

        std::list<State>::const_iterator next = boost::next(current);
        while (next != path.end() and lineOfSight(*current, *next, costmap) and
               std::sqrt(std::pow((next->x - current->x)*costmap.getResolution(), 2) + std::pow((next->y - current->y)*costmap.getResolution(),2)) < max_distance_)
            next++;

        out.push_back(*current);

        if (next == path.end())
            break;

        if (current == boost::prior(next))
        {
            ROS_ERROR("Current: (%d, %d, %d) Next:(%d, %d, %d)", current->x, current->y, cost(*current, costmap), next->x, next->y, cost(*next, costmap));
            assert(lineOfSight(*current, *next, costmap));
            break;
        }

        current = boost::prior(next);
    }

    out.push_back(path.back());

    return out;
}

bool astar::Astar::lineOfSight(const State& from, const State& to,  const costmap_2d::Costmap2D& costmap) const
{
    //bresenham algorithm

    int dx = std::abs(to.x - from.x);
    int dy = std::abs(to.y - from.y);

    int sx = (from.x < to.x)? 1: -1;
    int sy = (from.y < to.y)? 1: -1;

    int err = dx - dy;

    State current = from;
    while (true)
    {
        if (not admissible(current, costmap))
            return false;

        if (current == to)
            return true;

        int e2 = 2*err;
        if (e2 > -dy)
        {
            err -= dy;
            current.x += sx;
        }

        if (current == to)
            return admissible(current, costmap);

        if (e2 < dx)
        {
            err += dx;
            current.y += sy;
        }
    }
}
